# README #

Advanced Machine Learning 

### What is this repository for? ###

* This repository contains excercises to familiarize with machine learning concepts using Jupyter Notebook 
* The topics covered here are as follows:
	* Numpy introduction
	* KNN
	* Linear Regression
	* Logistic Regression
	* Regularization
	* Anomaly Detection
	* Recommender Systems
	* Clustering
	* Principal Component Analysis
	* Neural Network
	* Support Vector Machine
	

